package factory

import "gitlab.com/ljpx/trace"

// Factory defines the methods that any runtime instantiator must implement.
type Factory interface {
	Fabricate(name string) trace.Identifiable
}
