![](icon.png)

# factory

Package `factory` defines a type that can instantiate structs on-the-fly.

## Usage Example

The `factory` package is useful in situations where you need to deserialize
generic messages to multiple different types.

Consider the situation where we receive messages from a queue that, when
serialized, look something like the two examples below:

```json
{
    "type": "CreateUserCommand",
    "data": "{\"firstName\":\"John\",\"lastName\":\"Smith\",\"age\":45}"
}
```

```json
{
    "type": "RemoveUserCommand",
    "data": "{\"userID\": \"15bffdea7ae1138c6c57ad5fb01ed08a\"}"
}
```

Both of these examples could be unmarshaled into this same structure:

```go
type Message struct {
    Type string `json:"type"`
    Data string `json:"data"`
}
```

The problem is that the `data` field becomes difficult to use - we can't tell
what type we want to instantiate to unmarshal the `data` field.

For this, we define our two command types (that must implement
`trace.Identifiable`):

```go
type CreateUserCommand struct {
    FirstName string `json:"firstName"`
    LastName  string `json:"lastName"`
    Age       int    `json:"age"`
}

func (c *CreateUserCommand) Identify() string {
    return "CreateUserCommand"
}
```

```go
type RemoveUserCommand struct {
    UserID id.ID `json:"userID"`
}

func (c *RemoveUserCommand) Identify() string {
    return "RemoveUserCommand"
}
```

And we blueprint them into a factory:

```go
factory := factory.New()
factory.Blueprint(&CreateUserCommand{})
factory.Blueprint(&RemoveUserCommand{})
```

Now we can unmarshal our messages:

```go
// We acquire `msg` from somewhere...
var msg *Message

// `Produce` returns a *T, where T is the type that was blueprinted.
cmd := factory.Produce(msg.Type)

// We unmarshal and can now pass `cmd` to something that understands it.
err := json.Unmarshal([]byte(msg.Data), cmd)
...
createUserCommand := cmd.(*CreateUserCommand)
fmt.Printf("First Name: %v\n", createUserCommand.FirstName)
```
