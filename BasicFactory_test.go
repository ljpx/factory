package factory

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ljpx/trace"
)

func TestBasicFactoryBadBlueprint(t *testing.T) {
	// Arrange.
	factory := New()

	// Act and Assert.
	require.Panics(t, func() {
		factory.Blueprint(badBlueprintType(func() {}))
	})
}

func TestBasicFactoryPointerBlueprint(t *testing.T) {
	// Arrange.
	factory := New()

	// Act and Assert.
	require.NotPanics(t, func() {
		factory.Blueprint(&goodBlueprintType{})
	})
}

func TestBasicFactoryStructBlueprint(t *testing.T) {
	// Arrange.
	factory := New()

	// Act and Assert.
	require.NotPanics(t, func() {
		factory.Blueprint(goodBlueprintType{})
	})
}

func TestBasicFactoryFabricateUnknownName(t *testing.T) {
	// Arrange.
	factory := New()

	// Act and Assert.
	require.Panics(t, func() {
		factory.Fabricate("goodBlueprintType")
	})
}

func TestBasicFactoryFabricatePointer(t *testing.T) {
	// Arrange.
	factory := New()
	factory.Blueprint(&goodBlueprintType{})

	// Act.
	inst := factory.Fabricate("goodBlueprintType")

	// Assert.
	require.IsType(t, &goodBlueprintType{}, inst)
}

func TestBasicFactoryFabricateStruct(t *testing.T) {
	// Arrange.
	factory := New()
	factory.Blueprint(goodBlueprintType{})

	// Act.
	inst := factory.Fabricate("goodBlueprintType")

	// Assert.
	require.IsType(t, &goodBlueprintType{}, inst)
}

func TestBasicFactoryUsageExampleScenario(t *testing.T) {
	// Arrange.
	factory := New()
	factory.Blueprint(&createUserCommand{})
	factory.Blueprint(&removeUserCommand{})

	// Act.
	msg1 := &message{
		Type: "CreateUserCommand",
		Data: "{\"firstName\":\"John\",\"lastName\":\"Smith\",\"age\":45}",
	}

	msg2 := &message{
		Type: "RemoveUserCommand",
		Data: "{\"userID\": \"15bffdea7ae1138c6c57ad5fb01ed08a\"}",
	}

	cmd1 := factory.Fabricate(msg1.Type)
	cmd2 := factory.Fabricate(msg2.Type)

	err := json.Unmarshal([]byte(msg1.Data), cmd1)
	require.Nil(t, err)

	err = json.Unmarshal([]byte(msg2.Data), cmd2)
	require.Nil(t, err)

	// Assert.
	createUserCommand, ok := cmd1.(*createUserCommand)
	require.True(t, ok)
	require.Equal(t, "John", createUserCommand.FirstName)
	require.Equal(t, "Smith", createUserCommand.LastName)
	require.Equal(t, 45, createUserCommand.Age)

	removeUserCommand, ok := cmd2.(*removeUserCommand)
	require.True(t, ok)
	require.Equal(t, "15bffdea7ae1138c6c57ad5fb01ed08a", removeUserCommand.UserID)
}

// -----------------------------------------------------------------------------

type badBlueprintType func()

var _ trace.Identifiable = badBlueprintType(func() {})

func (b badBlueprintType) Identify() string {
	return "badBlueprintType"
}

type goodBlueprintType struct{}

var _ trace.Identifiable = goodBlueprintType{}

func (b goodBlueprintType) Identify() string {
	return "goodBlueprintType"
}

type message struct {
	Type string `json:"type"`
	Data string `json:"data"`
}

type createUserCommand struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Age       int    `json:"age"`
}

func (c *createUserCommand) Identify() string {
	return "CreateUserCommand"
}

type removeUserCommand struct {
	UserID string `json:"userID"`
}

func (c *removeUserCommand) Identify() string {
	return "RemoveUserCommand"
}
