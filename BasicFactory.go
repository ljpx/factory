package factory

import (
	"fmt"
	"reflect"
	"sync"

	"gitlab.com/ljpx/trace"
)

// BasicFactory implements the Factory interface.
type BasicFactory struct {
	namedTypes map[string]reflect.Type
	mx         *sync.RWMutex
}

var _ MutableFactory = &BasicFactory{}

// New creates a new BasicFactory, returned as a MutableFactory.
func New() MutableFactory {
	return &BasicFactory{
		namedTypes: make(map[string]reflect.Type),
		mx:         &sync.RWMutex{},
	}
}

// Blueprint stores type information for the provided identifiable object.
func (f *BasicFactory) Blueprint(obj trace.Identifiable) {
	f.mx.Lock()
	defer f.mx.Unlock()

	objType := reflect.TypeOf(obj)
	if objType.Kind() == reflect.Ptr {
		objType = objType.Elem()
	}

	if objType.Kind() != reflect.Struct {
		panicWithBadBlueprint(reflect.TypeOf(obj))
	}

	f.namedTypes[obj.Identify()] = objType
}

// Fabricate creates an instance of the type associated with the provided name.
func (f *BasicFactory) Fabricate(name string) trace.Identifiable {
	f.mx.RLock()
	defer f.mx.RUnlock()

	objType, ok := f.namedTypes[name]
	if !ok {
		panicWithUnknownName(name)
	}

	return reflect.New(objType).Interface().(trace.Identifiable)
}

func panicWithBadBlueprint(t reflect.Type) {
	panic(fmt.Errorf("the provided blueprint has an unsupported type, type must be like `*T` or `T`, where T is a struct, but looks more like `%v` (Kind: %v)", t, t.Kind()))
}

func panicWithUnknownName(name string) {
	panic(fmt.Errorf("the provided name %q has not been blueprinted in this factory", name))
}
