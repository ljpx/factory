package factory

import "gitlab.com/ljpx/trace"

// MutableFactory defines the methods that a factory capable of registering new
// types must implement.
type MutableFactory interface {
	Factory

	Blueprint(obj trace.Identifiable)
}
